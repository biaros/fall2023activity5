package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args)
    {
        Book[] books = new Book[5];
        books[0] = new Book("Chain of Gold", "Cassandra Clare");
        books[1] = new ElectronicBook("Chain of Iron", "Cassandra Clare", 24);
        books[2] = new Book("Chain of Thorns", "Cassandra Clare");
        books[3] = new ElectronicBook("Six of Crows", "Leigh Bardugo", 20);
        books[4] = new ElectronicBook("The Atlas Six", "Olivie Blake", 15);

        for (Book b : books)
        {
            System.out.println(b);
        }
    }
}
