package polymorphism.inheritance;

public class ElectronicBook extends Book {
    private double numberBytes;

    public double getNumberBytes()
    {
        return(this.numberBytes);
    }

    public ElectronicBook(String title, String author, double numberBytes)
    {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public String toString()
    {
        String s = super.toString();
        s = s + ", Bytes: " + numberBytes;
        return(s);
    }
}
